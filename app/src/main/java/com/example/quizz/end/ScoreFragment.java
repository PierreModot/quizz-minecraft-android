package com.example.quizz.end;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.quizz.R;

public class ScoreFragment extends Fragment {

    private String score;
    private String message;

    public ScoreFragment(){}

    public ScoreFragment(int score, String mod){
        System.out.println(mod);
        if (mod.equals("tenQuestions")) {
            if (score == 10) message = "Score parfait ! Trop fort(e) !";
            else if (score >= 5) message = "Pas mal !";
            else if (score >= 3) message = "Il faut encore vous entrainer";
            else if (score == 0) message = "Nul";
            else message = "Aïe...";

            this.score = score + "/" + 10;


        } else {
            if (score >= 30) message = "TU ES IMBATABLE !";
            else if (score >= 20) message = "Très impressionnant !";
            else if (score >= 10) message = "Wow !";
            else if (score >= 5) message = "Pas mal !";
            else if (score >= 3) message = "Il faut encore vous entrainer";
            else if (score == 0) message = "Nul";
            else message = "Aïe...";

            if (score > 1) this.score = score + " bonnes reponses !";
            else this.score = score + " bonne reponse";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.activity_fragment_score, container, false);

        TextView tvScore = (TextView) rootView.findViewById(R.id.score);
        TextView tvMessage = (TextView) rootView.findViewById(R.id.messageScore);

        tvScore.setText(score);
        tvMessage.setText(message);



        return rootView;
    }

}
