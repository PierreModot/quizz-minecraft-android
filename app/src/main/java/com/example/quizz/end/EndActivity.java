package com.example.quizz.end;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import com.example.quizz.MainActivity;
import com.example.quizz.R;
import com.example.quizz.difficulty.DifficultyActivity;
import com.example.quizz.quiz.QuizActivity;
import com.example.quizz.theme.ThemeActivity;

public class EndActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        Button bReturn = findViewById(R.id.buttonRetour);
        Button bShare = findViewById(R.id.buttonShare);
        Button bRestart = findViewById(R.id.buttonRestart);

        int score = getIntent().getIntExtra("SCORE", 0);
        System.out.println(score);
        String mod = getIntent().getStringExtra("MOD");

        String theme = getIntent().getStringExtra("THEME");

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_score, new ScoreFragment(score, mod)).commit();






        bReturn.setOnClickListener(view -> {
            Intent MainActivity = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(MainActivity);
        });

        bShare.setOnClickListener(view -> {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);

            if(mod.equals("withoutFault")){
                sendIntent.putExtra(Intent.EXTRA_TEXT,"J'ai réussis à enchainer " + score + " questions en mode sans faute sur le theme "+ theme + "! Essaie de faire mieux en téléchargeant" +
                         "l'application Minecraft Quiz" );
            }else{
                sendIntent.putExtra(Intent.EXTRA_TEXT, "J'ai fait un score de " + score + "/10 sur le thème  " + theme +
                        " ! N'hésite pas à tester ton niveau toi aussi en jouant sur notre application !");
            }

            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        });

        bRestart.setOnClickListener(view -> {
            Intent difficultyActivity = new Intent(getApplicationContext(), DifficultyActivity.class);
            difficultyActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            difficultyActivity.putExtra("MOD",mod);
            difficultyActivity.putExtra("THEME", theme);
            startActivity(difficultyActivity);
        });
    }
}
