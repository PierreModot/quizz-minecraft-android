package com.example.quizz.difficulty;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.quizz.MainActivity;
import com.example.quizz.R;
import com.example.quizz.quiz.QuizActivity;


import java.util.ArrayList;

public class DifficultyActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficulty);

        String theme = getIntent().getStringExtra("THEME");
        String mod = getIntent().getStringExtra("MOD");




        Intent quizIntent = new Intent(getApplicationContext(), QuizActivity.class);

        Button easy = findViewById(R.id.easy);
        Button medium = findViewById(R.id.medium);
        Button hard = findViewById(R.id.hard);

        Button start = findViewById(R.id.start);


        addDifficulty(easy, quizIntent, "easy");
        addDifficulty(medium, quizIntent, "medium");
        addDifficulty(hard, quizIntent, "hard");



        start.setOnClickListener(view -> {
            quizIntent.putExtra("THEME", theme);
            quizIntent.putExtra("MOD", mod);

            addDifficulty(easy, quizIntent, "easy");
            addDifficulty(medium, quizIntent, "medium");
            addDifficulty(hard, quizIntent, "hard");

            easy.setBackgroundColor(Color.parseColor("#8F8080"));
            medium.setBackgroundColor(Color.parseColor("#8F8080"));
            hard.setBackgroundColor(Color.parseColor("#8F8080"));

            startActivity(quizIntent);

                System.out.println(mod + "difficulty");
        });
    }



    public void addDifficulty(Button button, Intent quizIntent, String difficulty){
        button.setOnClickListener(view -> {
            button.setBackgroundColor(Color.parseColor("#574C4C"));
            MainActivity.difficulties.add(difficulty);
            //changer de couleur
            removeDifficulty(button, quizIntent, difficulty);
        });
    }

    public void removeDifficulty(Button button, Intent quizIntent, String difficulty){
        button.setOnClickListener(view -> {
            button.setBackgroundColor(Color.parseColor("#8F8080"));
            MainActivity.difficulties.remove(MainActivity.difficulties.indexOf(difficulty));
            //remettre la couleur de base
            addDifficulty(button, quizIntent, difficulty);
        });
    }


}
