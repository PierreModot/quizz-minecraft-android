package com.example.quizz.quiz;



import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.quizz.MainActivity;
import com.example.quizz.R;
import com.example.quizz.end.EndActivity;

import java.util.ArrayList;
import java.util.Collections;

public class QuizActivity extends AppCompatActivity {

    int points = 0;
    int question = 0;



    String theme;
    String difficulty;
    String mod;

    /*
    @Override
    public void onBackPressed() {
        difficulties = new ArrayList<String>();
        //finishActivity();
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);



        theme = getIntent().getStringExtra("THEME");
        difficulty = getIntent().getStringExtra("DIFFICULTY");
        mod = getIntent().getStringExtra("MOD");

        TextView test = findViewById(R.id.test);

        //System.out.println(difficulties);

        QuestionList QL = new QuestionList(MainActivity.difficulties,theme,getApplicationContext());

        MainActivity.difficulties = new ArrayList<String>();

/*
        for (Question question : QL.getQuestionList() ){
            System.out.println(question.getQuestion());
            System.out.println(question.getDifficulty());
        }
*/


        newQuestion(QL);
    }



    private void newQuestion(QuestionList QL) {
        if (QL.getQuestionList().size() > 0) /*vérifier qu'il reste des questions*/ {
            TextView nubQuestion = findViewById(R.id.tvNumberQuestion);
            nubQuestion.setText("Question " + (question+1));

            //choisir une question au hasard
            Collections.shuffle(QL.getQuestionList());
            int numQuestion = (int) Math.random() * (QL.getQuestionList().size());
            Question question = QL.getQuestionList().get(numQuestion);
            QL.getQuestionList().remove(numQuestion);

            //récupérer les composants de la page
            TextView tvQuestion = findViewById(R.id.test);
            EditText input = findViewById(R.id.input);
            Button answer1 = findViewById(R.id.awnser1);
            Button answer2 = findViewById(R.id.awnser2);
            Button answer3 = findViewById(R.id.awnser3);
            Button answer4 = findViewById(R.id.awnser4);
            ImageView image = findViewById(R.id.imageView);
            LinearLayout llQuestion = findViewById(R.id.question);
            LinearLayout llReponses = findViewById(R.id.réponses);

            //remettre la couleur de base aux boutons
            answer1.setBackgroundColor(Color.parseColor("#574C4C"));
            answer2.setBackgroundColor(Color.parseColor("#574C4C"));
            answer3.setBackgroundColor(Color.parseColor("#574C4C"));
            answer4.setBackgroundColor(Color.parseColor("#574C4C"));


            if(this.question != 0) /*vérifier que ce n'est pas la première questoin*/ {
                //déclarer les animations
                Animation anim_start = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.swipe_start);
                Animation anim_end = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.swipe_end);

                System.out.println(mod + "quiz");

                //jouer le début de l'animation
                llQuestion.startAnimation(anim_start);
                llReponses.startAnimation(anim_start);

                //bloquer les boutons
                answer1.setClickable(false);
                answer2.setClickable(false);
                answer3.setClickable(false);
                answer4.setClickable(false);

                anim_start.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {

                        //jouer la fin de l'animation
                        llQuestion.startAnimation(anim_end);
                        llReponses.startAnimation(anim_end);

                        anim_end.setAnimationListener(new Animation.AnimationListener(){
                            @Override
                            public void onAnimationStart(Animation arg0) {
                            }
                            @Override
                            public void onAnimationRepeat(Animation arg0) {
                            }
                            @Override
                            public void onAnimationEnd(Animation arg0) {
                                //débloquer les boutons
                                answer1.setClickable(true);
                                answer2.setClickable(true);
                                answer3.setClickable(true);
                                answer4.setClickable(true);
                            }
                        });

                        //afficher la question
                        tvQuestion.setText(question.getQuestion());

                        //récuperer le string de la bonne réponse
                        String goodAnswer = question.getAwnsers().get(0);

                        //mélanger les réponses
                        Collections.shuffle(question.getAwnsers());

                        //cacher ou afficher l'image
                        image.setVisibility(View.GONE);
                        if (!question.getImage().isEmpty()) {

                            int photo = getApplicationContext().getResources().getIdentifier(question.getImage(), "drawable", getApplicationContext().getPackageName());
                            image.setImageResource(photo);

                            image.setVisibility(View.VISIBLE);
                        } else {
                            image.setVisibility(View.GONE);
                        }

                        //arfficher le bon nombre de réponse et les afficher dans les boutons
                        if (question.getAwnsers().size() == 4) {
                            input.setVisibility(View.GONE);
                            answer1.setText(question.getAwnsers().get(0));
                            answer2.setVisibility(View.VISIBLE);
                            answer2.setText(question.getAwnsers().get(1));
                            answer3.setVisibility(View.VISIBLE);
                            answer3.setText(question.getAwnsers().get(2));
                            answer4.setVisibility(View.VISIBLE);
                            answer4.setText(question.getAwnsers().get(3));
                        } else if (question.getAwnsers().size() == 2) {
                            input.setVisibility(View.GONE);
                            answer1.setText(question.getAwnsers().get(0));
                            answer2.setVisibility(View.VISIBLE);
                            answer2.setText(question.getAwnsers().get(1));
                            answer3.setVisibility(View.GONE);
                            answer4.setVisibility(View.GONE);
                        } else if (question.getAwnsers().size() == 1) {
                            answer1.setText("Valider");
                            input.setText("");
                            input.setVisibility(View.VISIBLE);
                            answer2.setVisibility(View.GONE);
                            answer3.setVisibility(View.GONE);
                            answer4.setVisibility(View.GONE);
                        }

                        //définir le bon et les mauvais boutons
                        Button bGoodAnswer;
                        if (answer1.getText() == goodAnswer){
                            bGoodAnswer = answer1;
                        }else if (answer2.getText() == goodAnswer){
                            bGoodAnswer = answer2;
                        }else if (answer3.getText() == goodAnswer){
                            bGoodAnswer = answer3;
                        }else {
                            bGoodAnswer = answer4;
                        }

                        if(question.getAwnsers().size() == 1){
                            setAnswerInput(answer1,input, question.getAwnsers().get(0).toString(), QL);
                        }else{
                            setBadAnswer(answer1, QL, bGoodAnswer );
                            setBadAnswer(answer2, QL, bGoodAnswer );
                            setBadAnswer(answer3, QL, bGoodAnswer );
                            setBadAnswer(answer4, QL, bGoodAnswer );
                            setGoodAnswer(bGoodAnswer, QL);
                        }


                        //désactiver les boutons
                        answer1.setClickable(false);
                        answer2.setClickable(false);
                        answer3.setClickable(false);
                        answer4.setClickable(false);
                    }
                });

            } else {
                tvQuestion.setText(question.getQuestion());

                String goodAnswer = question.getAwnsers().get(0);
                Collections.shuffle(question.getAwnsers());

                image.setVisibility(View.GONE);
                if (!question.getImage().isEmpty()) {

                    int photo = getApplicationContext().getResources().getIdentifier(question.getImage(), "drawable", getApplicationContext().getPackageName());
                    image.setImageResource(photo);

                    image.setVisibility(View.VISIBLE);
                } else {
                    image.setVisibility(View.GONE);
                }

                if (question.getAwnsers().size() == 4) {
                    input.setVisibility(View.GONE);
                    answer1.setText(question.getAwnsers().get(0));
                    answer2.setVisibility(View.VISIBLE);
                    answer2.setText(question.getAwnsers().get(1));
                    answer3.setVisibility(View.VISIBLE);
                    answer3.setText(question.getAwnsers().get(2));
                    answer4.setVisibility(View.VISIBLE);
                    answer4.setText(question.getAwnsers().get(3));
                } else if (question.getAwnsers().size() == 2) {
                    input.setVisibility(View.GONE);
                    answer1.setText(question.getAwnsers().get(0));
                    answer2.setVisibility(View.VISIBLE);
                    answer2.setText(question.getAwnsers().get(1));
                    answer3.setVisibility(View.GONE);
                    answer4.setVisibility(View.GONE);
                } else if (question.getAwnsers().size() == 1) {
                    answer1.setText("Valider");
                    input.setText("");
                    input.setVisibility(View.VISIBLE);
                    answer2.setVisibility(View.GONE);
                    answer3.setVisibility(View.GONE);
                    answer4.setVisibility(View.GONE);
                }

                Button bGoodAnswer;
                if (answer1.getText() == goodAnswer){
                    bGoodAnswer = answer1;
                }else if (answer2.getText() == goodAnswer){
                    bGoodAnswer = answer2;
                }else if (answer3.getText() == goodAnswer){
                    bGoodAnswer = answer3;
                }else {
                    bGoodAnswer = answer4;
                }

                if(question.getAwnsers().size() == 1){
                    setAnswerInput(answer1,input, question.getAwnsers().get(0).toString(), QL);
                }else{
                    setBadAnswer(answer1, QL, bGoodAnswer );
                    setBadAnswer(answer2, QL, bGoodAnswer );
                    setBadAnswer(answer3, QL, bGoodAnswer );
                    setBadAnswer(answer4, QL, bGoodAnswer );
                    setGoodAnswer(bGoodAnswer, QL);
                }


            }


        }
        else {
            endActivity();
        }


    }

    public void setAnswerInput(Button button, EditText input, String answer, QuestionList QL){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(answer.equalsIgnoreCase(input.getText().toString())){
                    Toast.makeText(getApplicationContext(),"Bonne réponse !", Toast.LENGTH_SHORT).show();
                    points++;
                } else  Toast.makeText(getApplicationContext(),"Mauvaise réponse !", Toast.LENGTH_SHORT).show();
                question++;
                if (question < 10) {
                    newQuestion(QL);

                }else{
                    endActivity();
                }
            }


        });
    }

    private void setGoodAnswer(Button button, QuestionList QL){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               
                  Toast.makeText(getApplicationContext(),"Bonne réponse !", Toast.LENGTH_SHORT).show();

              
                points++;
                question++;
                if (question < 10) {
                    newQuestion(QL);

                }else{
                    endActivity();
                }
            }
        });
    }

    private void setBadAnswer(Button button, QuestionList QL, Button bGoodAnswer){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                         
                Toast.makeText(getApplicationContext(),"Mauvaise réponse !", Toast.LENGTH_SHORT).show();



                if (mod.equals("tenQuestions") ){
                    question++;
                    if (question < 10) {
                        newQuestion(QL);
                    }
                    else{
                        endActivity();
                    }
                } else{

                    endActivity();
                    finish();
                }



            }
        });
    }

    private void endActivity() {
        Intent endActivity = new Intent(getApplicationContext(), EndActivity.class);
        endActivity.putExtra("SCORE", points);
        endActivity.putExtra("THEME", theme);
        endActivity.putExtra("DIFFICULTY", difficulty);
        endActivity.putExtra("MOD", mod);
        startActivity(endActivity);
    }


}
