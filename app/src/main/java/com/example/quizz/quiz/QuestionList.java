package com.example.quizz.quiz;

import android.content.Context;

import com.example.quizz.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Array;
import java.util.ArrayList;

public class QuestionList {
    private ArrayList<Question> questionList;

    public QuestionList(ArrayList<String> difficulty, String theme, Context applicationContext){
        questionList = new ArrayList<Question>();

        if(theme.equals("general")){
           construireListAll(difficulty, applicationContext);
        }else  construireList(difficulty, theme, applicationContext);
    }

    public ArrayList<Question> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(ArrayList<Question> questionList) {
        this.questionList = questionList;
    }

    public void construireList(ArrayList<String> difficulty, String theme, Context context){
        try {
            // Récupération du json
            JSONArray jsonArray = new JSONArray(getJSONFromRaw(context));
            // Récupération des questions
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject JSONquestion = jsonArray.getJSONObject(i);
                String questionDifficulty = JSONquestion.getString("difficulty");
                ArrayList<String> themes = JSONtoArray(JSONquestion.getJSONArray("theme"));

                if (difficulty.contains(questionDifficulty) && themes.contains(theme)){
                    String question = JSONquestion.getString("question");
                    String image = JSONquestion.getString("_comment");
                    ArrayList<String> awnsers = JSONtoArray(JSONquestion.getJSONArray("reponses"));
                    questionList.add(new Question(question,awnsers,themes,questionDifficulty,image));
                    System.out.println(image);
                }

            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void construireListAll(ArrayList<String> difficulty, Context context){
        try {
            // Récupération du json
            JSONArray jsonArray = new JSONArray(getJSONFromRaw(context));
            // Récupération des questions
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject JSONquestion = jsonArray.getJSONObject(i);
                String questionDifficulty = JSONquestion.getString("difficulty");
                ArrayList<String> themes = JSONtoArray(JSONquestion.getJSONArray("theme"));

                if (difficulty.contains(questionDifficulty)){
                    String question = JSONquestion.getString("question");
                    String image = JSONquestion.getString("_comment");
                    ArrayList<String> awnsers = JSONtoArray(JSONquestion.getJSONArray("reponses"));
                    questionList.add(new Question(question,awnsers,themes,questionDifficulty,image));
                    System.out.println(image);
                }

            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //pour transformer les JSONArray en ArrayList jsp si c'est la meilleure des façons mais ça fait le taff
    public ArrayList<String> JSONtoArray(JSONArray jsonArray) throws JSONException {
        ArrayList<String> array = new ArrayList<String>();
        for (int i=0; i<jsonArray.length(); i++){
            array.add(jsonArray.getString(i));
        }
        return array;
    }

    private static String getJSONFromRaw(Context context) {
        String json = null;
        try {
            InputStream is = context.getResources().openRawResource(R.raw.questions);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private static String getJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("questions.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}

