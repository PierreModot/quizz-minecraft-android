package com.example.quizz.quiz;

import java.util.ArrayList;

public class Question {
    private String question;
    private ArrayList<String> awnsers; // la première sera la bonne réponse
    private ArrayList<String> themes;
    private String difficulty;
    private String image;

    public Question(String question, ArrayList<String> awnsers, ArrayList<String> themes, String difficulty){
        this.question = question;
        this.awnsers = awnsers;
        this.themes = themes;
        this.difficulty = difficulty;
    }

    public Question(String question, ArrayList<String> awnsers, ArrayList<String> themes, String difficulty, String image){
        this.question = question;
        this.awnsers = awnsers;
        this.themes = themes;
        this.difficulty = difficulty;
        this.image = image;
    }

    public String getQuestion(){
        return question;
    }

    public ArrayList<String> getAwnsers(){
        return awnsers;
    }

    public ArrayList<String> getThemes(){
        return themes;
    }

    public String getDifficulty(){
        return difficulty;
    }

    public String getImage(){
        return image;
    }




}
