package com.example.quizz.theme;

import com.example.quizz.R;

import java.util.ArrayList;

public class ThemeItemList extends ArrayList<ThemeItem> {

    public ThemeItemList(){

        add(new ThemeItem("general", R.drawable.tout));
        add(new ThemeItem("mob hostiles", R.drawable.mob));
        add(new ThemeItem("redstone", R.drawable.redstone));
        add(new ThemeItem("biome",R.drawable.biome));

    }
}
