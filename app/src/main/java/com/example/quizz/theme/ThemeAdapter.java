package com.example.quizz.theme;

import static androidx.core.content.ContextCompat.startActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.quizz.R;
import com.example.quizz.difficulty.DifficultyActivity;

public class ThemeAdapter extends BaseAdapter {


    private Context context;
    private ThemeItemList themeItemList;
    private LayoutInflater inflater;

    public ThemeAdapter(Context context, ThemeItemList themeItemList){
        this.context = context;
        this.themeItemList = themeItemList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return themeItemList.size();
    }

    @Override
    public ThemeItem getItem(int i) {
        return themeItemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.adapter, null);

        ThemeItem currenItem = getItem(i);
        String itemName = currenItem.getName();
        int itemPicture = currenItem.getPicture();
        Button itemBDixQuestions = currenItem.getBDixQuestions();
        Button itemBSansFaute = currenItem.getBSansFaute();

        ImageView imageTheme = view.findViewById(R.id.icon_theme);
        TextView textTheme = view.findViewById(R.id.icon_name);
        Button bDixQuestionsTheme = view.findViewById(R.id.bDixQuestions);
        Button bSansFauteTheme = view.findViewById(R.id.bSansFaute);

        bDixQuestionsTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent difficultyIntent = new Intent(context, DifficultyActivity.class);
                difficultyIntent.putExtra("THEME", currenItem.getName());
                difficultyIntent.putExtra("MOD", "tenQuestions");

                context.startActivity(difficultyIntent);
            }
        });

        bSansFauteTheme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent difficultyIntent = new Intent(context, DifficultyActivity.class);
                difficultyIntent.putExtra("THEME", currenItem.getName());
                difficultyIntent.putExtra("MOD", "withoutFault");

                context.startActivity(difficultyIntent);
            }
        });

        imageTheme.setImageResource(itemPicture);
        textTheme.setText(itemName);



        return view;
    }



}
