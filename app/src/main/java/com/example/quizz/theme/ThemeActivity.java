package com.example.quizz.theme;

import static androidx.core.content.ContextCompat.startActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.quizz.R;
import com.example.quizz.difficulty.DifficultyActivity;

public class ThemeActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);

        ThemeItemList listItem = new ThemeItemList();
        ListView listView = findViewById(R.id.theme_list_view);
        ThemeAdapter themeAdapter = new ThemeAdapter(this, listItem);
        listView.setAdapter(themeAdapter);


    }




}
