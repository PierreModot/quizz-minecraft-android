package com.example.quizz.theme;

import android.content.Intent;
import android.widget.Button;

import com.example.quizz.difficulty.DifficultyActivity;

public class ThemeItem {

    private String name;
    private int picture;
    private Button bSansFaute;
    private Button bDixQuestions;

    public ThemeItem(String name, int picture){
        this.name = name;
        this.picture = picture;


    }

    public String getName(){return name;}
    public int getPicture(){return picture;}
    public Button getBSansFaute(){return bSansFaute;}
    public Button getBDixQuestions(){return bDixQuestions;}

}
