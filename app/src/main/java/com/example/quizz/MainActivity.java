package com.example.quizz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.example.quizz.theme.ThemeActivity;
import com.example.quizz.wiki.WikiActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static ArrayList<String> difficulties = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button play = findViewById(R.id.playButton);
        Button wiki = findViewById(R.id.wikiButton);

        Animation anim_fade_in = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);

        play.startAnimation(anim_fade_in);

        wiki.startAnimation(anim_fade_in);

        play.setOnClickListener(view -> {
            Intent themeActivity = new Intent(getApplicationContext(), ThemeActivity.class);
            startActivity(themeActivity);
        });



        wiki.setOnClickListener(view -> {
            Intent wikiActivity = new Intent(getApplicationContext(), WikiActivity.class);
            startActivity(wikiActivity);
        });
    }
}