## Projet Quizz Android Studio
Projet dans le cadre du DUT informatique, 2ème année, 2021/2022, S4.
Conception et développement d'applications mobiles.

Ce dépot est réalisé 3 ans après la fin du projet.
Certaines fonctionnalités ou textures sont manquantes

## Participants
Ce projet a été réalisé en groupe composé de :
- Martin Baras
- Alexandre Coulet
- Pierre Modot

## Description
Quizz sur le thème de Minecraft

### Fonctionnalités
- Choix du thème
- Choix du mode du jeu
    - mode sans faute : continue les questions jusqu'a ce que le joueur ait faux.
    - mode 10 questions : pose 10 questions. 
- Choix de la difficulté

## Usage
Lancer le projet sur un android simulé depuis android studio
